#!/bin/sh

cd core/core
echo " " && echo ">> PWD =" $PWD
npm i && npm run build && npm publish && git add . && git commit -am "Updated" && git push
cd ../..

cd lambda/lambda
echo " " && echo ">> PWD =" $PWD
npm i && ng build && git add . && git commit -am "Updated" && git push
cd ../..

cd client/client
echo " " && echo ">> PWD =" $PWD
npm i && ng build && git add . && git commit -am "Updated" && git push
cd ../..

